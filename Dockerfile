FROM registry.gitlab.com/iamadriaperez/docker-archlinux:latest

RUN pacman -Syu --noconfirm \
    gsettings-desktop-schemas \
    gtk2 \
    imagemagick \
    java-environment \
    libxrender \
    libxtst \
    socat \
    ttf-dejavu \
    unzip \
    xorg-server \
    xorg-server-xvfb

#TODO: remove skipchecksums whenever possible
RUN aur-install.sh ib-tws --skipchecksums && \
    aur-install.sh ibc --skipchecksums

COPY start.sh /start.sh
RUN mkdir /var/run/xvfb/ && \
    cp /etc/ibc/edemo.ini /etc/ibc/conf.ini && \
    chmod a+x /start.sh

EXPOSE 4003

CMD ["/start.sh"]
